package com.sub.email.service.controller;

import com.sub.email.service.client.EmailSender;
import com.sub.email.service.model.EmailNotificationRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@AllArgsConstructor
@RequestMapping(
        value = "/notifications",
        consumes = MediaType.APPLICATION_JSON_VALUE
)
@RestController
public class EmailServiceController {

    private final EmailSender emailSender;

    /**
     * The best way to do this is using some Queue tool
     * */
    @PostMapping
    public ResponseEntity<?> notification(@RequestBody final EmailNotificationRequest request) {
        log.info("Request received to send a notification, email {}", request.getEmail());
        emailSender.sendEmailNotification(request);
        return ResponseEntity.ok().build();
    }

}
