package com.sub.email.service.client;

import com.sub.email.service.model.EmailNotificationRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailSender {

    public void sendEmailNotification(final EmailNotificationRequest request) {

        log.info("Sending email notification to: {}, email {}", request.getFirstName(), request.getEmail());
        getEmailConfiguration();
        successMessage();

    }

    private void getEmailConfiguration() {
        log.info("Email sever configuration completed");
    }

    private void successMessage() {
        log.info("Email send successfully...");
    }
}
