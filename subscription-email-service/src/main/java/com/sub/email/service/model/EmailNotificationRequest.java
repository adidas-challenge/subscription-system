package com.sub.email.service.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class EmailNotificationRequest {
    private String firstName;
    private String email;

    public EmailNotificationRequest(String firstName, String email) {
        this.firstName = firstName;
        this.email = email;
    }
}
