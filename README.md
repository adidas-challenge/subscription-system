# SUBSCRIPTION SERVER

GitLab Repository

SSH
```bash
$ git clone git@gitlab.com:adidas-challenge/subscription-system.git
```

HTTPS
```bash
$ git clone https://gitlab.com/adidas-challenge/subscription-system.git
```

##################################################
## SUBSCRIPTION SYSTEM ##
##################################################

### Built With
- Java 8                    [JAVA](http://www.oracle.com/technetwork/java/javaee/overview/index.html)
- Spring Boot 2.x.z         [SPRING-BOOT](https://spring.io/projects/spring-boot)
- Spring Framework 5.x.z    [SPRING](https://spring.io/projects/spring-framework)
- Gradle 7.x.z              [GRADLE](https://gradle.org/)
- Spring Data JPA 2.x.z     [SPRING-DATA](https://spring.io/projects/spring-data-jpa)
- H2 DB 1.x.z               [H2](https://www.h2database.com/html/download.html)
  * In memory database - Practical and simple
- JUnit 4.x.z               [JUNIT](https://junit.org/junit4/)
- Mockito 3.x.z             [MOCKITO](https://site.mockito.org/)
  * Mocking framework for testing
- JaCoCo 0.8.z              [JACOCO](https://www.eclemma.org/jacoco/)
  * Code coverage library - In order to know what functional code was tested
- SonarQube 8.x.z           [SONARQUBE](https://www.sonarqube.org/downloads/)
  * Code quality metrics - Track and improve code
- Lombok 1.x.z              [LOMBOK](https://projectlombok.org/setup/gradle)
  *  The library replaces boilerplate code with easy-to-use annotations
- Spring HATEOAS 1.x.z      [HATEOAS](https://spring.io/projects/spring-hateoas)
  *  Ease creating REST representations that follow the HATEOAS principle
- Swagger 3-x-z             [SWAGGER](https://swagger.io/)
  * API Documentation
- Swagger UI 2.x.z          [SWAGGER-UI](https://swagger.io/tools/swagger-ui/)
  * visualize and interact with the API’s resources

### Prerequisites
- Install [Java8](https://openjdk.java.net/install/)
- Install [Gradle](https://gradle.org/)
- Install [Docker](https://www.docker.com/)

### Setting up
Steps in order to start the project:

```bash
./gradlew clean build
```

Create and load Docker images
```bash
docker-compose up -d --build --force-recreate
```

### How to Use
After running the Docker images,  check and use Public API

 To check SonarQube metrics and coverage results go to:

[SONARQUBE](http://localhost:9000)

By default, the Security is enabled, use the credentials:

    * USER: admin
    * PASSWORD: admin

Update your password, and you will go to main page,
in order to disable security and load the project
do the following steps:

    * Administration -> Security -> Go down and turn off the flag: Force User Auth

Load the project into SonarQube and review the information
```bash
./gradlew sonarqube
```

*************************************
When the application starts, created two test subscriptions with the following public ID's
* AD-SUB-TEST1
* AD-SUB-TEST2

For the API Documentation visit
[SWAGGER-UI](http://localhost:8030/v1/swagger-ui.html#/subscription-controller)

To call the API, you can use the Postman collection + environment added or 
here we have the different endpoints
### GET ALL THE SUBSCRIPTIONS
#### Method GET
#### Request
```bash
curl -X GET "http://localhost:8030/v1/subscriptions" -H "accept: application/json" -H "Content-Type: application/json"
```
#### Response
```
{
    "_embedded": {
        "subscriptionResponseList": [
            {
                "subscriptionId": "AD-SUB-TEST1",
                "status": "ACTIVE",
                "_links": {
                    "self": [
                        {
                            "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST1"
                        },
                        {
                            "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST1/cancel"
                        }
                    ],
                    "subscriptions": {
                        "href": "http://localhost:8030/v1/subscriptions"
                    }
                }
            },
            {
                "subscriptionId": "AD-SUB-TEST2",
                "status": "ACTIVE",
                "_links": {
                    "self": [
                        {
                            "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST2"
                        },
                        {
                            "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST2/cancel"
                        }
                    ],
                    "subscriptions": {
                        "href": "http://localhost:8030/v1/subscriptions"
                    }
                }
            }
        ]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8030/v1/subscriptions"
        }
    }
}
```

### GET SUBSCRIPTION DETAILS BY PUBLIC ID
#### Method GET
#### Request
```bash
curl -X GET "http://localhost:8030/v1/subscriptions/AD-SUB-TEST1" -H "accept: application/json" -H "Content-Type: application/json"
```
#### Response
```
{
    "subscriptionId": "AD-SUB-TEST1",
    "status": "ACTIVE",
    "email": "RAUL@EMAIL.COM",
    "firstName": "RAUL",
    "dateOfBirth": "1990-07-08",
    "gender": "MALE",
    "consent": true,
    "newsletterId": "ADIDAS2021MX",
    "_links": {
        "self": [
            {
                "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST1"
            },
            {
                "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST1/cancel"
            }
        ],
        "subscriptions": {
            "href": "http://localhost:8030/v1/subscriptions"
        }
    }
}
```
#### Response 404
```
{
    "code": 404,
    "message": "Subscription not found"
}
```

### CANCEL SUBSCRIPTION BY PUBLIC ID
#### Method DELETE
#### Request
```bash
curl -X DELETE "http://localhost:8030/v1/subscriptions/AD-SUB-TEST1/cancel" -H "accept: application/json" -H "Content-Type: application/json"
```
#### Response 200
```
{
    "subscriptionId": "AD-SUB-TEST1",
    "status": "CANCELLED",
    "_links": {
        "self": [
            {
                "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST2"
            },
            {
                "href": "http://localhost:8030/v1/subscriptions/AD-SUB-TEST2/cancel"
            }
        ],
        "subscriptions": {
            "href": "http://localhost:8030/v1/subscriptions"
        }
    }
}
```

#### Response 400
```
{
    "code": 400,
    "message": "Subscription status not valid"
}
```
#### Response 404
```
{
    "code": 404,
    "message": "Subscription not found"
}
```

### CREATE NEW SUBSCRIPTION
#### Method POST
#### Payload
```
{
  * "email": "string",                // valid email some@email.com
  * "dateOfBirth": "string",          // YYYY-MM-DD
  * "consent": true,                  // true or false
  * "newsletterId": "string"
  // OPTIONAL
  "firstName": "string", 
  "gender": "FEMALE",               // MALE, FEMALE, OTHER
}
```
#### Request
```bash
curl -X POST "http://localhost:8030/v1/subscriptions" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"consent\": true, \"dateOfBirth\": \"1990-01-01\", \"email\": \"manu@email.com\", \"firstName\": \"manu\", \"gender\": \"MALE\", \"newsletterId\": \"ADMX001\"}"
```
#### Response 200
```
{
  "subscriptionId": "AD-SUB-3ebff88c-e206-42e2-ab9e-6a748312b6b6",
  "status": "ACTIVE",
  "_links": {
    "self": [
      {
        "href": "http://localhost:8030/v1/subscriptions/AD-SUB-3ebff88c-e206-42e2-ab9e-6a748312b6b6"
      },
      {
        "href": "http://localhost:8030/v1/subscriptions/AD-SUB-3ebff88c-e206-42e2-ab9e-6a748312b6b6/cancel"
      }
    ],
    "subscriptions": {
      "href": "http://localhost:8030/v1/subscriptions"
    }
  }
}
```

#### Response 400 Missing required values
```
{
  "code": 400,
  "message": "must not be empty, Newsletter is required"
}
```


### SETTING DOWN

Stop Docker images
```bash
docker-compose down
```


#############################
## CI/CD Pipelines ##########
#############################

- [CI/CD-File](https://gitlab.com/adidas-challenge/subscription-system/-/blob/main/.gitlab-ci.yml)
- [PIPELINE](https://gitlab.com/adidas-challenge/subscription-system/-/pipelines/333791072)


#############################
## Kubernetes Config FIles ##
#############################

Based on Openshift scenario
- [K8-FILES](https://gitlab.com/adidas-challenge/subscription-system/-/tree/main/ocp_configs)