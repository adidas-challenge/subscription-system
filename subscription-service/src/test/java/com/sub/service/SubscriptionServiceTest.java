package com.sub.service;

import com.sub.client.EmailServiceClient;
import com.sub.exception.InvalidSubscriptionStatusException;
import com.sub.exception.SubscriptionNotFoundException;
import com.sub.mapper.SubscriptionDetailResponseMapper;
import com.sub.mapper.SubscriptionMapper;
import com.sub.model.entity.Subscription;
import com.sub.model.enums.Gender;
import com.sub.model.enums.Status;
import com.sub.repository.SubscriptionRepository;
import com.sub.request.CreateSubscriptionRequest;
import com.sub.response.SubscriptionDetailResponse;
import com.sub.response.SubscriptionListResponse;
import com.sub.response.SubscriptionResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import javax.persistence.PersistenceException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceTest {

    @InjectMocks
    private SubscriptionService subscriptionService;
    @Mock
    private SubscriptionRepository repositoryMock;
    @Mock
    private EmailServiceClient emailServiceClientMock;
    @Mock
    private SubscriptionMapper subscriptionMapperMock;
    @Mock
    private SubscriptionDetailResponseMapper detailResponseMapperMock;
    @Mock
    private RestTemplate restTemplate;

    private Subscription subscriptionOneMock;
    private Subscription subscriptionTwoMock;
    private Subscription subscriptionCancelMock;
    private List<Subscription> subscriptionsMock;
    private CreateSubscriptionRequest createRequestMock;

    @Before
    public void init() {
        subscriptionOneMock = mockSubscription("VANIA", "VANIA@GMAIL.COM", Gender.FEMALE);
        subscriptionTwoMock = mockSubscription("AZTECA", "AZTECA@MAYA.COM", Gender.OTHER);
        subscriptionCancelMock = mockSubscription("TESTY", "TESTY@MAYA.COM", Gender.FEMALE);
        subscriptionCancelMock.setStatus(Status.CANCELLED);

        subscriptionsMock = Arrays
                .asList(subscriptionOneMock, subscriptionTwoMock);


        createRequestMock = getCreateRequestMock();

        // MAPPERS
        Mockito.when(subscriptionMapperMock
                .buildSubscription(Mockito.any(CreateSubscriptionRequest.class)))
                .thenCallRealMethod();
        Mockito.when(detailResponseMapperMock
                .buildResponse(Mockito.any(Subscription.class)))
                .thenCallRealMethod();
        // MOCK EMAIL
        Mockito.doNothing()
                .when(emailServiceClientMock)
                .sendNotification(Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void findAllOkTest() {
        Mockito.when(repositoryMock.findAll())
                .thenReturn(subscriptionsMock);
        SubscriptionListResponse response = subscriptionService.getAll();
        Assert.assertFalse("The list contain results",
                response.getSubscriptions().isEmpty());
    }

    @Test(expected = PersistenceException.class)
    public void findAllExceptionTest() {
        Mockito.when(repositoryMock.findAll())
                .thenThrow(PersistenceException.class);
        subscriptionService.getAll();
    }

    @Test
    public void createOkTest() {
        Mockito.when(repositoryMock.save(Mockito.any(Subscription.class)))
                .thenReturn(subscriptionOneMock);

        SubscriptionResponse response = subscriptionService.create(createRequestMock);
        Assert.assertEquals(response.getSubscriptionId(), subscriptionOneMock.getPublicId());
        Assert.assertEquals("Valid status", Status.ACTIVE, response.getStatus());
    }


    @Test(expected = PersistenceException.class)
    public void createExceptionTest() {
        Mockito.doThrow(PersistenceException.class)
                .when(repositoryMock).save(Mockito.any(Subscription.class));

        subscriptionService.create(createRequestMock);
    }

    // TODO INVALID TEST DATA

    @Test
    public void findOneOkTest() {
        Mockito.when(repositoryMock.findByPublicId(Mockito.anyString()))
                .thenReturn(subscriptionTwoMock);

        SubscriptionDetailResponse response = subscriptionService
                .findById(subscriptionTwoMock.getPublicId());

        Assert.assertNotNull("Valid response", response);
        Assert.assertEquals("Same email ",
                response.getEmail(),
                subscriptionTwoMock.getEmail());
    }


    @Test(expected = PersistenceException.class)
    public void findOneExceptionTest() {
        Mockito.when(repositoryMock.findByPublicId(Mockito.anyString()))
                .thenThrow(PersistenceException.class);
        subscriptionService
                .findById(subscriptionTwoMock.getPublicId());
    }

    @Test(expected = SubscriptionNotFoundException.class)
    public void findOneNullTest() {
        Mockito.when(repositoryMock.findByPublicId(Mockito.anyString()))
                .thenReturn(null);
        subscriptionService
                .findById(subscriptionTwoMock.getPublicId());
    }

    @Test
    public void cancelOkTest() {
        Mockito.when(repositoryMock.findByPublicId(Mockito.anyString()))
                .thenReturn(subscriptionOneMock);

        Mockito.when(repositoryMock.save(Mockito.any(Subscription.class)))
                .thenReturn(subscriptionOneMock);

        SubscriptionResponse response = subscriptionService
                .cancel(subscriptionOneMock.getPublicId());

        Assert.assertNotNull("Valid response", response);
        Assert.assertEquals("Valid status ", Status.CANCELLED,
                response.getStatus());
    }


    @Test(expected = InvalidSubscriptionStatusException.class)
    public void cancelInvalidStatusTest() {
        Mockito.when(repositoryMock.findByPublicId(Mockito.anyString()))
                .thenReturn(subscriptionCancelMock);

        subscriptionService
                .cancel(subscriptionOneMock.getPublicId());
    }


    @Test(expected = SubscriptionNotFoundException.class)
    public void cancelExceptionTest() {
        Mockito.when(repositoryMock.findByPublicId(Mockito.anyString()))
                .thenReturn(null);
        subscriptionService
                .cancel(subscriptionTwoMock.getPublicId());
    }

    @Test(expected = PersistenceException.class)
    public void cancelDbExceptionTest() {
        Mockito.when(repositoryMock.findByPublicId(Mockito.anyString()))
                .thenReturn(subscriptionOneMock);
        Mockito.doThrow(PersistenceException.class)
                .when(repositoryMock).save(Mockito.any(Subscription.class));

        subscriptionService
                .cancel(subscriptionTwoMock.getPublicId());
    }


    Subscription mockSubscription(String name, String email, Gender gender) {
        Subscription subscription = new Subscription();
        subscription.setFirstName(name);
        subscription.setEmail(email);
        subscription.setGender(gender);
        subscription.setConsent(Boolean.TRUE);
        subscription.setDateOfBirth(LocalDate.now().withYear(1986));
        return subscription;
    }

    CreateSubscriptionRequest getCreateRequestMock() {
        CreateSubscriptionRequest request = new CreateSubscriptionRequest();
        request.setConsent(Boolean.TRUE);
        request.setEmail("test@email.com");
        request.setFirstName("Test");
        request.setGender(Gender.FEMALE);
        request.setDateOfBirth(LocalDate.now().withYear(1990));
        request.setNewsletterId("TEST1");
        return request;
    }
}
