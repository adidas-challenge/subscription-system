package com.sub.response;

import com.sub.model.enums.Gender;
import com.sub.model.enums.Status;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

/**
 * SubscriptionDetailResponse child class
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SubscriptionDetailResponse extends SubscriptionResponse {
    private String email;
    private String firstName;
    private LocalDate dateOfBirth;
    private Gender gender;
    private Boolean consent;
    private String newsletterId;

    public SubscriptionDetailResponse(String subscriptionId, Status status) {
        super(subscriptionId, status);
    }
}
