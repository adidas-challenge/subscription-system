package com.sub.response;

import lombok.Builder;
import lombok.Data;

/**
 * Error response class
 */
@Data
@Builder
public class ErrorResponse {
    private Integer code;
    private String message;
}
