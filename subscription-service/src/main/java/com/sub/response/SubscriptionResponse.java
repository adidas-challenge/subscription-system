package com.sub.response;

import com.sub.model.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * SubscriptionResponse class
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SubscriptionResponse {
    private String subscriptionId;
    private Status status;
}
