package com.sub.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * SubscriptionListResponse class
 */
@Builder
@Data
public class SubscriptionListResponse {
    private List<SubscriptionResponse> subscriptions;
}
