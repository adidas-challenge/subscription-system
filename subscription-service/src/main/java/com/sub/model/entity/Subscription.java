package com.sub.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sub.model.enums.Gender;
import com.sub.model.enums.Status;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.UUID;

import static com.sub.model.SubscriptionConstants.PREFIX_ID;
import static com.sub.model.enums.Status.ACTIVE;

/**
 * Subscription class
 */
@Data
@Entity
public class Subscription {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty
    @NotNull
    private String publicId;
    @Email
    @NotEmpty(message = "Email is required")
    @NotNull(message = "Email is required")
    private String email;
    private String firstName;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Past
    @NotNull(message = "Date of Birth is required")
    @Column(columnDefinition = "DATE")
    private LocalDate dateOfBirth;
    @NotNull
    private Boolean consent;
    @NotEmpty
    @NotNull(message = "Newsletter is required")
    private String newsletterId;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    public Subscription() {
        // Generate a public id
        this.publicId = PREFIX_ID + UUID.randomUUID();
        // By default all the subscriptions starts ACTIVE
        this.status = ACTIVE;
    }

    public Subscription(String publicId, String email, String firstName, Gender gender, LocalDate dateOfBirth, Boolean consent, String newsletterId) {
        this.publicId = publicId;
        this.status = ACTIVE;
        this.email = email;
        this.firstName = firstName;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.consent = consent;
        this.newsletterId = newsletterId;
    }
}
