package com.sub.model.enums;

/**
 * Enum class for Genders
 */
public enum Gender {
    FEMALE,
    MALE,
    OTHER
}
