package com.sub.model.enums;

/**
 * Enum class for Status
 */
public enum Status {
    ACTIVE,
    CANCELLED
}
