package com.sub.repository;

import com.sub.model.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * SubscriptionRepository class
 */
@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    Subscription findByPublicId(final String publicId);
}
