package com.sub.request;

import com.sub.model.enums.Gender;
import lombok.Data;

import java.time.LocalDate;

/**
 * CreateSubscriptionRequest class
 */
@Data
public class CreateSubscriptionRequest {
    private String email;
    private String firstName;
    private Gender gender;
    private LocalDate dateOfBirth;
    private Boolean consent;
    private String newsletterId;
}
