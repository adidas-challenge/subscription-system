package com.sub.exception;

import com.sub.response.ErrorResponse;
import lombok.AllArgsConstructor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    private static final String INTERNAL_SERVER_ERROR = "Unhandled exception in service";

    @ResponseBody
    @ExceptionHandler(SubscriptionBusinessException.class)
    public ResponseEntity<ErrorResponse> handleBusinessException(Exception ex) {
        logger.error(HttpStatus.NOT_FOUND.getReasonPhrase() + ": " + ex.getMessage());

        return new ResponseEntity<>(ErrorResponse.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .message(HttpStatus.NOT_FOUND.getReasonPhrase()).build(),
                HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ExceptionHandler({SubscriptionSystemException.class, RuntimeException.class})
    public ResponseEntity<ErrorResponse> handleSystemException(Exception ex) {
        logger.error(INTERNAL_SERVER_ERROR + ": " + ex.getMessage());

        return new ResponseEntity<>(ErrorResponse.builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(INTERNAL_SERVER_ERROR).build(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseBody
    @ExceptionHandler(SubscriptionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> subscriptionNotFoundHandler(SubscriptionNotFoundException ex) {
        logger.error(HttpStatus.NOT_FOUND.getReasonPhrase() + ": " + ex.getMessage());

        return new ResponseEntity<>(ErrorResponse.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .message(ex.getMessage()).build(),
                HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ExceptionHandler(InvalidSubscriptionStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> invalidStatusHandler(InvalidSubscriptionStatusException ex) {
        logger.error(HttpStatus.BAD_REQUEST.getReasonPhrase() + ": " + ex.getMessage());

        return new ResponseEntity<>(ErrorResponse.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(ex.getMessage()).build(),
                HttpStatus.BAD_REQUEST);
    }

    @Override
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        Map<String, Set<String>> errorsMap = fieldErrors.stream().collect(
                Collectors.groupingBy(FieldError::getField,
                        Collectors.mapping(FieldError::getDefaultMessage, Collectors.toSet())
                )
        );
        return new ResponseEntity<>(ErrorResponse.builder()
                .code(status.value())
                .message(errorsMap.toString()).build(),
                status);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<ErrorResponse> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
        logger.error(HttpStatus.BAD_REQUEST.getReasonPhrase() + ": " + ex.getMessage());

        List<String> errors = ex.getConstraintViolations().stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        return new ResponseEntity<>(ErrorResponse.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(errors.toString()).build(),
                HttpStatus.BAD_REQUEST);
    }

}
