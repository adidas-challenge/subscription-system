package com.sub.exception;

import com.sub.model.enums.Status;

public class InvalidSubscriptionStatusException extends RuntimeException {

    public InvalidSubscriptionStatusException(String subscriptionId, Status status) {
        super("Subscription status not valid, id: " + subscriptionId + ", Status: " + status);
    }
}
