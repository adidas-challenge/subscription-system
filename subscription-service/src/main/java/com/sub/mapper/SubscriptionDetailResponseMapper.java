package com.sub.mapper;

import com.sub.model.entity.Subscription;
import com.sub.response.SubscriptionDetailResponse;
import org.springframework.stereotype.Component;

/**
 * SubscriptionDetailResponseMapper
 */
@Component
public class SubscriptionDetailResponseMapper {

    /**
     * Method to map the subscription entity object to a Subscription details for the endpoint
     *
     * @param subscription the subscription entity object
     * @return The SubscriptionDetailResponse object
     */
    public SubscriptionDetailResponse buildResponse(final Subscription subscription) {
        // The SubscriptionDetailResponse is a child class from SubscriptionResponse
        SubscriptionDetailResponse response = new SubscriptionDetailResponse(subscription.getPublicId(), subscription.getStatus());
        response.setEmail(subscription.getEmail());
        response.setDateOfBirth(subscription.getDateOfBirth());
        response.setConsent(subscription.getConsent());
        response.setNewsletterId(subscription.getNewsletterId());
        // OPTIONAL VALUES
        response.setFirstName(subscription.getFirstName());
        response.setGender(subscription.getGender());
        return response;
    }
}
