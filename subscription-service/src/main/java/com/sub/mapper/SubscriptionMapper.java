package com.sub.mapper;

import com.sub.model.entity.Subscription;
import com.sub.request.CreateSubscriptionRequest;
import org.springframework.stereotype.Component;

/**
 * SubscriptionMapper class
 */
@Component
public class SubscriptionMapper {

    /**
     * Method to map the values from the request to the Subscription entity object
     * The validation will place when persist, if not valid, will generate an exception
     * Generate random id to be the public id
     * By default all the subscriptions starts ACTIVE
     *
     * @param createSubscriptionRequest the object form the request
     * @return Subscription entity object
     */
    public Subscription buildSubscription(final CreateSubscriptionRequest createSubscriptionRequest) {
        Subscription subscription = new Subscription();

        subscription.setEmail(createSubscriptionRequest.getEmail());
        subscription.setConsent(createSubscriptionRequest.getConsent());
        subscription.setDateOfBirth(createSubscriptionRequest.getDateOfBirth());
        subscription.setNewsletterId(createSubscriptionRequest.getNewsletterId());
        // OPTIONAL
        subscription.setFirstName(createSubscriptionRequest.getFirstName());
        subscription.setGender(createSubscriptionRequest.getGender());
        return subscription;
    }
}
