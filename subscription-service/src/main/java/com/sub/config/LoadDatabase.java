package com.sub.config;

import com.sub.model.entity.Subscription;
import com.sub.model.enums.Gender;
import com.sub.repository.SubscriptionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;

import static java.lang.Boolean.TRUE;

/**
 * LoadDatabase class
 */
@Slf4j
@Configuration
public class LoadDatabase {

    private static final String NEWSLETTER_ID = "ADIDAS2021MX";

    @Bean
    CommandLineRunner initDatabase(SubscriptionRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(getSubscription("AD-SUB-TEST1", "RAUL@EMAIL.COM", "RAUL")));
            log.info("Preloading " + repository.save(getSubscription("AD-SUB-TEST2","JESUS@EMAIL.COM", "JESUS")));
        };
    }

    private Subscription getSubscription(final String publicId, final String email, final String firstName) {
        return new Subscription(publicId, email, firstName, Gender.MALE, LocalDate.now().withYear(1990), TRUE, NEWSLETTER_ID);
    }
}
