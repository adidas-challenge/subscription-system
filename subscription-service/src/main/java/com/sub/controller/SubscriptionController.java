package com.sub.controller;

import com.sub.request.CreateSubscriptionRequest;
import com.sub.response.SubscriptionDetailResponse;
import com.sub.response.SubscriptionListResponse;
import com.sub.response.SubscriptionResponse;
import com.sub.service.SubscriptionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.sub.model.SubscriptionConstants.BASE_SUBSCRIPTIONS_URI;
import static com.sub.model.SubscriptionConstants.CANCEL_SUBSCRIPTIONS_URI;
import static com.sub.model.SubscriptionConstants.ID_SUBSCRIPTIONS_URI;

/**
 * SubscriptionController class
 */
@Slf4j
@AllArgsConstructor
@RequestMapping(
        value = BASE_SUBSCRIPTIONS_URI,
        consumes = MediaType.APPLICATION_JSON_VALUE
)
@RestController
public class SubscriptionController {

    /**
     * subscriptionService The subscription Service
     */
    private final SubscriptionService subscriptionService;

    /**
     * POST endpoint to create a new subscription
     *
     * @return ResponseEntity with the subscriptionId created
     */
    @PostMapping
    public ResponseEntity<SubscriptionResponse> addSubscription(
            @Valid @RequestBody final CreateSubscriptionRequest createSubscriptionRequest) {
        log.info("Subscription Add Request, Email: {}, DateOfBirth: {}",
                createSubscriptionRequest.getEmail(), createSubscriptionRequest.getDateOfBirth());
        SubscriptionResponse subscription = subscriptionService.create(createSubscriptionRequest);

        return ResponseEntity.status(HttpStatus.CREATED).body(subscription);
    }

    /**
     * DELETE endpoint to cancel an active subscription
     *
     * @return ResponseEntity with the subscriptionId canceled
     */
    @DeleteMapping(value = CANCEL_SUBSCRIPTIONS_URI)
    public ResponseEntity<SubscriptionResponse> cancelSubscription(@PathVariable final String subscriptionId) {
        log.info("Subscription Cancel Request, Id: {}", subscriptionId);
        SubscriptionResponse canceledSubscription = subscriptionService.cancel(subscriptionId);

        return ResponseEntity.ok(canceledSubscription);
    }

    /**
     * GET endpoint to retrieve the details from a subscription by the public id
     *
     * @param subscriptionId The public subscription id
     * @return SubscriptionDetailResponse The object with all the info from the subscription
     */
    @GetMapping(value = ID_SUBSCRIPTIONS_URI)
    public ResponseEntity<SubscriptionDetailResponse> getSubscription(@PathVariable final String subscriptionId) {
        log.info("Find One Subscription, Id: {}", subscriptionId);
        SubscriptionDetailResponse subscriptionResponse = subscriptionService.findById(subscriptionId);

        return ResponseEntity.ok(subscriptionResponse);
    }

    /**
     * GET endpoint to retrieve all the subscription from the DB
     *
     * @return SubscriptionListResponse The list of all the subscriptions
     */
    @GetMapping
    public ResponseEntity<SubscriptionListResponse> getAllSubscriptions() {
        log.info("Get all the subscriptions");
        SubscriptionListResponse subscriptions = subscriptionService.getAll();

        return ResponseEntity.ok(subscriptions);
    }
}
