package com.sub.service;

import com.sub.client.EmailServiceClient;
import com.sub.exception.InvalidSubscriptionStatusException;
import com.sub.exception.SubscriptionNotFoundException;
import com.sub.mapper.SubscriptionDetailResponseMapper;
import com.sub.mapper.SubscriptionMapper;
import com.sub.model.entity.Subscription;
import com.sub.repository.SubscriptionRepository;
import com.sub.request.CreateSubscriptionRequest;
import com.sub.response.SubscriptionDetailResponse;
import com.sub.response.SubscriptionListResponse;
import com.sub.response.SubscriptionResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;

import static com.sub.model.enums.Status.CANCELLED;

/**
 * SubscriptionService class
 */
@AllArgsConstructor
@Slf4j
@Service
public class SubscriptionService {

    /**
     * subscriptionRepository The Subscription Repository
     */
    private final SubscriptionRepository subscriptionRepository;
    /**
     * Client to generate the calls to the email service
     */
    private final EmailServiceClient emailServiceClient;
    /**
     * Mapper for the subscription object
     */
    private final SubscriptionMapper subscriptionMapper;
    /**
     * Mapper for the subscription details object
     */
    private final SubscriptionDetailResponseMapper subscriptionDetailResponseMapper;

    /**
     * Method to Create a subscription
     * 1.- Create the subscription object based on request
     * 2.- Store the new subscription
     * 2.1 - Validate that the request has the required fields (Email, Date of Birth and newsletter flag)
     * 2.1.1 If there is missing or invalid data will return an exception
     * 3.- Send the notification
     *
     * @param createSubscriptionRequest The request for create a subscription
     * @return Saved subscription
     * @throws PersistenceException error with the db
     */
    public SubscriptionResponse create(final CreateSubscriptionRequest createSubscriptionRequest) {
        log.info("Create subscription {}", createSubscriptionRequest);
        // map the values from the request to the Entity
        Subscription subscription = subscriptionMapper.buildSubscription(createSubscriptionRequest);

        try {
            subscription = subscriptionRepository.save(subscription);
        } catch (PersistenceException ex) {
            log.error("Database error when trying to save the subscription, id: {}, email: {}",
                    subscription.getPublicId(),
                    subscription.getEmail());
            throw ex;
        }
        log.info("Subscription created successfully");

        emailServiceClient.sendNotification(subscription.getFirstName(), subscription.getEmail());

        return SubscriptionResponse.builder()
                .subscriptionId(subscription.getPublicId())
                .status(subscription.getStatus())
                .build();
    }

    /**
     * Method to retrieve one transaction from db, and put the status as CANCELLED
     * 1.- Validate if the subscription exist
     * 2.- Validate the current status
     * 2.1.- If the status is already CANCELLED throws exception
     * 3.-Save the subscription with the new status
     * 4.- Send the notification
     *
     * @param subscriptionId the public subscription id
     * @return SubscriptionDetailResponse the subscription with the new status
     * @throws PersistenceException error with the db
     */
    public SubscriptionResponse cancel(final String subscriptionId) {
        // First we check if the subscription exist
        Subscription subscription = subscriptionRepository.findByPublicId(subscriptionId);

        if (subscription == null) {
            log.error("Subscription not found with the id: {}", subscriptionId);
            throw new SubscriptionNotFoundException(subscriptionId);
        }

        if (CANCELLED.equals(subscription.getStatus())) {
            // If the subscription is already cancelled do nothing
            log.error("Current status not valid for the cancel process, {}", subscription.getStatus());
            throw new InvalidSubscriptionStatusException(subscriptionId, subscription.getStatus());
        }
        // Change the status to CANCELLED and persist the subscription
        subscription.setStatus(CANCELLED);
        try {
            subscription = subscriptionRepository.save(subscription);
        } catch (PersistenceException ex) {
            log.error("Database error when trying to cancel the subscription, id: {}, email: {}",
                    subscription.getPublicId(),
                    subscription.getEmail());
            throw ex;
        }
        log.info("Subscription: {}, Cancelled successfully...", subscription.getPublicId());

        emailServiceClient.sendNotification(subscription.getFirstName(), subscription.getEmail());

        return SubscriptionResponse.builder()
                .subscriptionId(subscription.getPublicId())
                .status(subscription.getStatus())
                .build();
    }

    /**
     * Method to retrieve one transaction from db, using the public Id
     *
     * @param subscriptionId the public subscription id
     * @return SubscriptionDetailResponse all the details from the transaction
     * @throws PersistenceException error with the db
     */
    public SubscriptionDetailResponse findById(final String subscriptionId) {
        Subscription subscription;
        try {
            // Retrieve the subscription from db
            subscription = subscriptionRepository.findByPublicId(subscriptionId);
        } catch (PersistenceException ex) {
            log.error("Database error when trying to retrieve the subscription, id: {}", subscriptionId);
            throw ex;
        }
        // throw exception when there is no subscription
        if (subscription == null) {
            log.error("Subscription not found with the id: {}", subscriptionId);
            throw new SubscriptionNotFoundException(subscriptionId);
        }
        log.info("Subscription found successfully, email: {}", subscription.getEmail());
        return subscriptionDetailResponseMapper.buildResponse(subscription);
    }

    /**
     * Method to return all the transactions from the database
     *
     * @return All the subscriptions from the database
     * @throws PersistenceException error with the db
     */
    public SubscriptionListResponse getAll() {
        // Get all the subscription and map to the response object list
        List<SubscriptionResponse> subscriptions;
        try {
            subscriptions = subscriptionRepository.findAll()
                    .stream()
                    .map(subscription ->
                            SubscriptionResponse.builder()
                                    .subscriptionId(subscription.getPublicId())
                                    .status(subscription.getStatus())
                                    .build()
                    )
                    .collect(Collectors.toList());
        } catch (PersistenceException ex) {
            log.error("Database error when trying to retrieve all the subscription");
            throw ex;
        }
        log.info("Retrieved all the subscriptions successfully...");
        return SubscriptionListResponse.builder()
                .subscriptions(subscriptions)
                .build();
    }

}
