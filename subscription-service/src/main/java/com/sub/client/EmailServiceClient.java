package com.sub.client;

import com.sub.request.EmailNotificationRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * EmailServiceClient class
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class EmailServiceClient {

    private final RestTemplate restTemplate;

    @Value("${downstream.services.emailService.host}")
    private String emailServiceHost;
    @Value("${downstream.services.emailService.path}")
    private String emailServicePath;
    @Value("${downstream.services.emailService.auth}")
    private String emailServiceAuth;

    /**
     * Method to send the request in order to create an email notification
     * Here is possible use some tools like kafka, SQS
     */
    @Async
    public void sendNotification(final String firstName, final String email) {
        log.info("Send notification to: {}, email: {}", firstName, email);
        EmailNotificationRequest notificationRequest = new EmailNotificationRequest(firstName, email);

        HttpEntity<EmailNotificationRequest> requestEntity =
                new HttpEntity<>(notificationRequest, buildHttpHeaders());

        try {
            restTemplate.postForEntity(emailServiceHost + emailServicePath, requestEntity, String.class);
            log.info("Message send SUCCESSFULLY!");
        } catch (RestClientException e) {
            log.error("Error trying to send the notification. ", e);
        }

    }

    /**
     * Build the http entity for the call
     *
     * @return HttpHeader
     */
    private HttpHeaders buildHttpHeaders() {
        String encodedCredentials = new String(Base64.encodeBase64(emailServiceAuth.getBytes()));

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + encodedCredentials);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }
}
