package com.sub.api.exception;

import com.sub.api.enums.Status;

public class InvalidSubscriptionStatusException extends RuntimeException {

    public InvalidSubscriptionStatusException(String subscriptionId, Status status) {
        super("Subscription status not valid, id: " + subscriptionId + ", Status: " + status);
    }
}
