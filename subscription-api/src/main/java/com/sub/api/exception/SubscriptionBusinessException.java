package com.sub.api.exception;

/**
 *
 */
public class SubscriptionBusinessException extends Exception {

    private static final long serialVersionUID = 9071243526992305188L;

    public SubscriptionBusinessException() {
        super("Could not process invalid request");
    }

    public SubscriptionBusinessException(String message) {
        super(message);
    }
}
