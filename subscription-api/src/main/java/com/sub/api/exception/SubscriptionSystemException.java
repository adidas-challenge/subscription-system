package com.sub.api.exception;

/**
 *
 */
public class SubscriptionSystemException extends Exception {

    private static final long serialVersionUID = -5700374122964812949L;

    public SubscriptionSystemException() {
        super("Internal Server Error");
    }

    public SubscriptionSystemException(String message) {
        super(message);
    }
}
