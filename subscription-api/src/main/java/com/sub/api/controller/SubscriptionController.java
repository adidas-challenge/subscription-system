package com.sub.api.controller;

import com.sub.api.client.SubscriptionServiceClient;
import com.sub.api.exception.SubscriptionBusinessException;
import com.sub.api.exception.SubscriptionSystemException;
import com.sub.api.model.SubscriptionDetailModelAssembler;
import com.sub.api.model.SubscriptionModelAssembler;
import com.sub.api.request.CreateSubscriptionRequest;
import com.sub.api.response.SubscriptionDetailResponse;
import com.sub.api.response.SubscriptionResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static com.sub.api.constants.SubscriptionConstants.BASE_SUBSCRIPTIONS_URI;
import static com.sub.api.constants.SubscriptionConstants.CANCEL_SUBSCRIPTIONS_URI;
import static com.sub.api.constants.SubscriptionConstants.ID_SUBSCRIPTIONS_URI;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * SubscriptionController class
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(
        value = BASE_SUBSCRIPTIONS_URI,
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class SubscriptionController {

    private final SubscriptionServiceClient subscriptionServiceClient;
    private final SubscriptionModelAssembler subscriptionModelAssembler;
    private final SubscriptionDetailModelAssembler subscriptionDetailModelAssembler;

    /**
     * POST Endpoint to create a new subscription
     *
     * @param createSubscriptionRequest the subscription request
     * @return EntityModel
     */
    @ApiOperation(value = "Create a new subscription")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            code = 201,
                            message = "Subscription Created Successfully",
                            response = SubscriptionResponse.class
                    ),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 409, message = "Conflict client error"),
                    @ApiResponse(code = 500, message = "Unhandled Exception in service")
            }
    )
    @PostMapping
    public ResponseEntity<?> addSubscription(
            @ApiParam("The creation object")
            @Valid @RequestBody CreateSubscriptionRequest createSubscriptionRequest) {

        SubscriptionResponse subscription = subscriptionServiceClient.create(createSubscriptionRequest);
        EntityModel<SubscriptionResponse> entityModel = subscriptionModelAssembler.toModel(subscription);

        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    /**
     * DELETE endpoint to cancel a subscription
     *
     * @param subscriptionId the subscription public id
     * @return EntityModel
     */
    @ApiOperation(value = "Cancel the subscription")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            code = 200,
                            message = "Subscription canceled successfully",
                            response = SubscriptionResponse.class
                    ),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 404, message = "Subscription not found"),
                    @ApiResponse(code = 409, message = "Conflict client error"),
                    @ApiResponse(code = 500, message = "Unhandled Exception in service")
            }
    )
    @DeleteMapping(value = CANCEL_SUBSCRIPTIONS_URI)
    public EntityModel<SubscriptionResponse> cancelSubscription(@ApiParam("Subscription public ID")
                                                                @PathVariable String subscriptionId)
            throws SubscriptionSystemException {

        SubscriptionResponse canceledSubscription = subscriptionServiceClient.cancel(subscriptionId);

        return subscriptionModelAssembler.toModel(canceledSubscription);
    }

    /**
     * GET Method to retrieve the subscriptions details
     *
     * @param subscriptionId the subscription public id
     * @return EntityModel
     */
    @ApiOperation(value = "Get subscription details")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            code = 200,
                            message = "",
                            response = SubscriptionDetailResponse.class
                    ),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 404, message = "Subscription not found"),
                    @ApiResponse(code = 409, message = "Conflict client error"),
                    @ApiResponse(code = 500, message = "Unhandled Exception in service")
            }
    )
    @GetMapping(value = ID_SUBSCRIPTIONS_URI)
    public EntityModel<SubscriptionDetailResponse> getSubscription(@PathVariable String subscriptionId)
            throws SubscriptionSystemException {

        SubscriptionDetailResponse detailResponse = subscriptionServiceClient.findById(subscriptionId);

        return subscriptionDetailModelAssembler.toModel(detailResponse);
    }

    /**
     * GET Method to retrieve all the subscriptions
     *
     * @return CollectionModel
     */
    @ApiOperation(value = "Get all the subscriptions")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Subscriptions retrieved successfully"),
                    @ApiResponse(code = 400, message = "Bad Request"),
                    @ApiResponse(code = 404, message = "Subscription not found"),
                    @ApiResponse(code = 409, message = "Conflict client error"),
                    @ApiResponse(code = 500, message = "Unhandled Exception in service")
            }
    )
    @GetMapping
    public CollectionModel<EntityModel<SubscriptionResponse>> getAllSubscriptions()
            throws SubscriptionSystemException {

        List<EntityModel<SubscriptionResponse>> subscriptions = subscriptionServiceClient.getAll().stream()
                .map(subscriptionModelAssembler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(subscriptions, linkTo(methodOn(SubscriptionController.class)
                .getAllSubscriptions()).withSelfRel());
    }

}
