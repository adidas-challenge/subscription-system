package com.sub.api.enums;

public enum Status {
    ACTIVE,
    CANCELLED
}
