package com.sub.api.enums;

public enum Gender {
    FEMALE,
    MALE,
    OTHER
}
