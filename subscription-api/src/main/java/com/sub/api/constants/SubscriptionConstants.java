package com.sub.api.constants;

public class SubscriptionConstants {
    public static final String WHITELIST = "^[A-Za-z0-9|\\/ ._~-]*$";

    public static final String BASE_SUBSCRIPTIONS_URI = "/subscriptions";
    public static final String ID_SUBSCRIPTIONS_URI = "/{subscriptionId}";
    public static final String CANCEL_SUBSCRIPTIONS_URI = ID_SUBSCRIPTIONS_URI + "/cancel";

    public static final String SUBSCRIPTIONS = "subscriptions";
}
