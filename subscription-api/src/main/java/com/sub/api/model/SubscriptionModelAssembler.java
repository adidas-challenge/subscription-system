package com.sub.api.model;

import com.sub.api.controller.SubscriptionController;
import com.sub.api.response.SubscriptionResponse;
import lombok.SneakyThrows;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static com.sub.api.constants.SubscriptionConstants.SUBSCRIPTIONS;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class SubscriptionModelAssembler implements RepresentationModelAssembler<SubscriptionResponse, EntityModel<SubscriptionResponse>> {

    @SneakyThrows
    @Override
    public EntityModel<SubscriptionResponse> toModel(SubscriptionResponse subscription) {
        return EntityModel.of(subscription,
                WebMvcLinkBuilder.linkTo(methodOn(SubscriptionController.class).getSubscription(subscription.getSubscriptionId())).withSelfRel(),
                linkTo(methodOn(SubscriptionController.class).getAllSubscriptions()).withRel(SUBSCRIPTIONS),
                linkTo(methodOn(SubscriptionController.class).cancelSubscription(subscription.getSubscriptionId())).withSelfRel());
    }

    @Override
    public CollectionModel<EntityModel<SubscriptionResponse>> toCollectionModel(Iterable<? extends SubscriptionResponse> subscriptions) {
        return RepresentationModelAssembler.super.toCollectionModel(subscriptions);
    }
}
