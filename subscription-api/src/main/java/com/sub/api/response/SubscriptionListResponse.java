package com.sub.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SubscriptionListResponse {
    private List<SubscriptionResponse> subscriptions;
}
