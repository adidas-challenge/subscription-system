package com.sub.api.response;

import com.sub.api.enums.Gender;
import com.sub.api.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class SubscriptionDetailResponse extends SubscriptionResponse {
    private String email;
    private String firstName;
    private LocalDate dateOfBirth;
    private Gender gender;
    private Boolean consent;
    private String newsletterId;

    public SubscriptionDetailResponse(String subscriptionId, Status status) {
        super(subscriptionId, status);
    }
}
