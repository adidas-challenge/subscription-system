package com.sub.api.response;

import com.sub.api.enums.Status;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel("Subscription response object")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SubscriptionResponse {
    @ApiModelProperty("Public subscription ID")
    private String subscriptionId;
    @ApiModelProperty("Subscription status")
    private Status status;
}
