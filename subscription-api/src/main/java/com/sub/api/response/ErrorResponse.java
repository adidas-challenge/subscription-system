package com.sub.api.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@ApiModel("Error response object")
@Data
@Builder
public class ErrorResponse {
    @ApiModelProperty("HTTP Status code")
    private Integer code;
    @ApiModelProperty("HTTP Message")
    private String message;
}
