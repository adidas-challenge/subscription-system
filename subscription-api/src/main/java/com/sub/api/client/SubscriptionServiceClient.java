package com.sub.api.client;

import com.sub.api.exception.SubscriptionSystemException;
import com.sub.api.request.CreateSubscriptionRequest;
import com.sub.api.response.SubscriptionDetailResponse;
import com.sub.api.response.SubscriptionListResponse;
import com.sub.api.response.SubscriptionResponse;
import com.sub.api.utils.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

/**
 * SubscriptionService
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class SubscriptionServiceClient {

    @Value("${downstream.services.subscriptionService.host}")
    private String subscriptionServiceHost;
    @Value("${downstream.services.subscriptionService.path}")
    private String subscriptionServicePath;

    private final RestTemplateUtil restTemplateUtil;
    private final RestTemplate restTemplate;

    private static final String SLASH = "/";
    private static final String SUBSCRIPTION_ID = "{subscriptionId}";
    private static final String CANCEL_URI = "/cancel";

    private final HttpEntity<?> httpEntity = new HttpEntity<>(buildHttpHeaders());

    /**
     * Method for call the subscription service to create a new one
     *
     * @param createSubscriptionRequest Payload to create a subscription
     * @return subscription created
     */
    public SubscriptionResponse create(final CreateSubscriptionRequest createSubscriptionRequest) {
        log.info("Create new subscription: {}", createSubscriptionRequest);
        HttpEntity<CreateSubscriptionRequest> requestEntity =
                new HttpEntity<>(createSubscriptionRequest, buildHttpHeaders());

        return restTemplate.postForObject(
                subscriptionServiceHost + subscriptionServicePath,
                requestEntity,
                SubscriptionResponse.class
        );
    }

    /**
     * Method for call the subscription service to update the status to CANCELLED using the public id
     *
     * @param subscriptionId The public subscription id to be canceled
     * @return subscription canceled with the new status
     * @throws SubscriptionSystemException the system exception
     */
    public SubscriptionResponse cancel(final String subscriptionId) throws SubscriptionSystemException {
        log.info("Cancel subscription, id: {}", subscriptionId);
        SubscriptionResponse response;

        response = restTemplateUtil.performRestCall(
                subscriptionServiceHost, subscriptionServicePath + SLASH + SUBSCRIPTION_ID + CANCEL_URI,
                HttpMethod.DELETE,
                httpEntity,
                SubscriptionResponse.class,
                Collections.singletonMap("subscriptionId", subscriptionId));

        return response;
    }

    /**
     * Method for get the subscription by the public id
     *
     * @param subscriptionId the public id
     * @return SubscriptionDetailResponse
     * @throws SubscriptionSystemException the system exception
     */
    public SubscriptionDetailResponse findById(final String subscriptionId) throws SubscriptionSystemException {
        log.info("Find subscription, id: {}", subscriptionId);

        return restTemplateUtil.performRestCall(
                subscriptionServiceHost, subscriptionServicePath + SLASH + SUBSCRIPTION_ID,
                HttpMethod.GET, httpEntity, SubscriptionDetailResponse.class,
                Collections.singletonMap("subscriptionId", subscriptionId));
    }

    /**
     * Method use for call creation to return a list of subscriptions
     *
     * @return a list of subscriptions
     * @throws SubscriptionSystemException the system exception
     */
    public List<SubscriptionResponse> getAll() throws SubscriptionSystemException {
        log.info("Find all the subscriptions");
        SubscriptionListResponse response = restTemplateUtil.performRestCall(subscriptionServiceHost, subscriptionServicePath,
                HttpMethod.GET, httpEntity, SubscriptionListResponse.class, Collections.emptyMap());
        return response.getSubscriptions();
    }

    /**
     * Build the http entity for the call
     *
     * @return HttpHeader
     */
    private HttpHeaders buildHttpHeaders() {
        String credentials = "adminuser:password";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + encodedCredentials);
        httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

}
