package com.sub.api.request;

import com.sub.api.enums.Gender;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

@ApiModel(description = "Subscription Creation Request")
@Data
public class CreateSubscriptionRequest {
    @ApiModelProperty(notes = "Email should have a valid format")
    private String email;
    @ApiModelProperty(notes = "First Name is optional")
    private String firstName;
    @ApiModelProperty(notes = "Gender can be FEMALE, MALE or OTHER")
    private Gender gender;
    @ApiModelProperty(notes = "Date of birth, should be a valid date")
    private LocalDate dateOfBirth;
    @ApiModelProperty(notes = "Consent flag")
    private Boolean consent;
    @ApiModelProperty(notes = "Id of the campaign")
    private String newsletterId;
}
