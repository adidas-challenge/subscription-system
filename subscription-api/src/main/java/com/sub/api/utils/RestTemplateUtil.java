package com.sub.api.utils;

import com.sub.api.exception.SubscriptionBusinessException;
import com.sub.api.exception.SubscriptionSystemException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Slf4j
@AllArgsConstructor
@Component
public class RestTemplateUtil {

    private final RestTemplate restTemplate;

    public <T> T performRestCall(@NotNull String clientHost, @NotNull String clientPath, @NotNull HttpMethod httpMethod,
                                 HttpEntity<?> entity, @NotNull Class<T> clazz, Map<String, ?> params) throws SubscriptionSystemException {

        if (clientHost == null || clientPath == null) {
            log.error("URL info is null");
            throw new IllegalArgumentException("URL info is null");
        }

        if (httpMethod == null) {
            log.error("httpMethod is null");
            throw new IllegalArgumentException("httpMethod is null");
        }

        if (clazz == null) {
            log.error("class is null");
            throw new IllegalArgumentException("class is null");
        }

        String url = UriComponentsBuilder.fromHttpUrl(clientHost).path(clientPath).build().toString();
        log.info("Calling service... {}, method: {}", url, httpMethod);
        ResponseEntity<T> responseEntity;
        try {
            responseEntity = restTemplate.exchange(url, httpMethod, entity, clazz, params);
        } catch (HttpClientErrorException e) {
            log.error("Client error when calling the service.", e);
            throw e;
        } catch (HttpServerErrorException e) {
            log.error("Server error when calling the service", e);
            throw e;
        } catch (Exception e) {
            log.error("Unhandled exception when calling the service", e);
            throw new SubscriptionSystemException();
        }

        if (null == responseEntity.getBody()) {
            log.error("No response from the server");
            throw new SubscriptionSystemException("No response from the server");
        }
        log.info("Service response: code {}, Body: {}", responseEntity.getStatusCode(), responseEntity.getBody());
        return responseEntity.getBody();
    }
}
